<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_history', function (Blueprint $table) {
            $table->id();
            $table->dateTime('date_time');
            
            $table->unsignedBigInteger('item_category_id');
            $table->foreign('item_category_id')
                    ->references('id')->on('item_category')
                    ->onDelete('cascade');
                    
            $table->unsignedBigInteger('goods_id');
            $table->foreign('goods_id')
                    ->references('id')->on('goods')
                    ->onDelete('cascade');
            $table->char('type', 1)->comment('0: in , 1: out, 2: adjusment');
            $table->string('remaks', 150)->nullable(true);
            $table->integer('before_amount')->default(0);
            $table->integer('out')->default(0);
            $table->integer('in')->default(0);
            $table->integer('adjus')->default(0);
            $table->integer('after_amount')->default(0);
            
            $table->unsignedBigInteger('create_by');
            $table->foreign('create_by')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_history');
    }
}
