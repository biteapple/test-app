<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/register', 'App\Http\Controllers\AuthController@register');
Route::post('/login', 'App\Http\Controllers\AuthController@login');
Route::get('/user', 'App\Http\Controllers\AuthController@user');

// Items Category : Master
Route::post('/item-category/add', 'App\Http\Controllers\ItemCategoryController@add');
Route::get('/item-category/get', 'App\Http\Controllers\ItemCategoryController@get');

// Goods : Master
Route::post('/goods/add', 'App\Http\Controllers\GoodsController@add');
Route::get('/goods/get', 'App\Http\Controllers\GoodsController@get');

// Transaction and Reporting
Route::post('/transaction/add', 'App\Http\Controllers\TransactionReportingController@add');
Route::get('/transaction/getDetail', 'App\Http\Controllers\TransactionReportingController@getDetail');
Route::get('/transaction/getStock', 'App\Http\Controllers\TransactionReportingController@getStock');


