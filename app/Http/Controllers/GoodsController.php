<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ValidateGoods;
use App\Models\Goods;

class GoodsController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function add(ValidateGoods $request){
        $user = Goods::create([
            'code'              =>  $request->code,
            'name'              =>  $request->name,
            'stock'             =>  $request->stock,
            'item_category_id'  =>  $request->item_category_id,
        ]); 
        return [
            "code"      =>  "200",
            "messages"  =>  "success"
        ]; 
    }

    public function get(request $request){
        $data = Goods::get(); 
        return $data->toJson(); 
    }
}
