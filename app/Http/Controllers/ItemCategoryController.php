<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ValidateItemCategory;
use App\Models\ItemCategory;

class ItemCategoryController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function add(ValidateItemCategory $request){
        $user = ItemCategory::create([
            'code' => $request->code,
            'name' => $request->name,
        ]); 
        return [
            "code"      =>  "200",
            "messages"  =>  "success"
        ]; 
    }

    public function get(request $request){
        $data = ItemCategory::get(); 
        return $data->toJson(); 
    }

}
