<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ValidateItemHistory;
use App\Models\ItemHistory;
use App\Models\Goods;

class TransactionReportingController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function add(ValidateItemHistory $request){
        
        $in             = 0;
        $out            = 0;
        $adjusment      = 0;
        $beforeAmount   = 0;
        $affterAmount   = 0;

        $dataGood = Goods::find($request->goods_id);
        
        // Type => 0: in , 1: out, 2: adjusment
        if ($request->type == 0) {
            # code...
            $in             = $request->in;
            $beforeAmount   = $dataGood->stock;
            $affterAmount   = $dataGood->stock + $request->in;
        }elseif ($request->type == 1) {
            # code...
            $out            = $request->out;
            $beforeAmount   = $dataGood->stock;
            $affterAmount   = $dataGood->stock - $request->out;

            if ($affterAmount < 0) {
                # code...
                return [
                    "code"      =>  "500",
                    "messages"  =>  "stock minus"
                ]; 
            }
        }elseif ($request->type == 2) {
            # code...
            $adjusment      = $request->adjusment;
            $beforeAmount   = $dataGood->stock;
            $affterAmount   = $request->adjusment;
        }else {
            return [
                "code"      =>  "404",
                "messages"  =>  "Not Found Type"
            ]; 
        }
        $dataItemHistory = ItemHistory::create([
            'date_time'         =>  $request->date_time,
            'item_category_id'  =>  $request->item_category_id,
            'goods_id'          =>  $request->goods_id,
            'type'              =>  $request->type,
            'remaks'            =>  $request->remaks,
            'before_amount'     =>  $beforeAmount,
            'out'               =>  $out,
            'in'                =>  $in,
            'adjus'             =>  $adjusment,
            'after_amount'      =>  $affterAmount,
            'create_by'         =>  $request->create_by,
        ]); 
        
        $dataGood->stock = $affterAmount;
        $dataGood->save();

        return [
            "code"      =>  "200",
            "messages"  =>  "success"
        ]; 
    }
    
    public function getStock(request $request){
        $data = Goods::get();
        return $data->toJson();
    }

    public function getDetail(request $request){

        $data = null;
        
        if(!$request->filled('start_date') && !$request->filled('end_date')){
            return[
                "code"      =>  "500",
                "messages"  =>  "error code"
            ];
        }
        if ($request->type == 0) {
            # code...
            $data = ItemHistory::where('date_time', '>=', $request->start_date)->where('date_time', '<=', $request->end_date);
            if ($request->filled('reporting') && $request->reporting <= 2) {
                $data = $data->where("type", $request->reporting)->get();
            }else {
                $data = $data->get();
            }
        }elseif ($request->type == 1){
            $data = ItemHistory::where('date_time', '>=', $request->start_date)->where('date_time', '<=', $request->end_date);
            if ($request->filled('reporting') && $request->reporting <= 2) {
                $data = $data->where("type", $request->reporting)->get();
            }else {
                $data = $data->get();
            }
        }elseif ($request->type == 2){
            $startTime=strtotime($request->start_date);
            $endTime=strtotime($request->end_date);
            $startMonth=date("m",$startTime);
            $startYear=date("Y",$startTime);
            $endMonth=date("m",$endTime);
            $endYear=date("Y",$endTime);
            $data = ItemHistory::whereRaw("DATE_FORMAT(date_time, '%Y-%m') >= ?", [$startYear."-".$startMonth])
                                ->whereRaw("DATE_FORMAT(date_time, '%Y-%m') <= ?", [$endYear."-".$endMonth]);
            if ($request->filled('reporting') && $request->reporting <= 2) {
                $data = $data->where("type", $request->reporting)->get();
            }else {
                $data = $data->get();
            }
        }elseif ($request->type == 3){
            $startTime=strtotime($request->start_date);
            $endTime=strtotime($request->end_date);
            $startYear=date("Y",$startTime);
            $endYear=date("Y",$endTime);
            $data = ItemHistory::whereYear('date_time', '>=', $startYear)->whereYear('date_time', '<=', $endYear);
            if ($request->filled('reporting') && $request->reporting <= 2) {
                $data = $data->where("type", $request->reporting)->get();
            }else {
                $data = $data->get();
            }
        }else{
            if ($request->filled('reporting') && $request->reporting <= 2) {
                $data = ItemHistory::where("type", $request->reporting)->get();
            }else {
                $data = ItemHistory::get();
            }
        } 
        return $data->toJson(); 
    }
}
