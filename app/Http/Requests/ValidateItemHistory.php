<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateItemHistory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'date_time'         =>      "required",
            'item_category_id'  =>      "required",
            'goods_id'          =>      "required",
            'type'              =>      "required",
            'out'               =>      "required",
            'in'                =>      "required",
            'adjusment'         =>      "required",
            'create_by'         =>      "required",
        ];
    }
}
