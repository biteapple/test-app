<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Goods extends Model
{
    use HasFactory;
    protected $table= 'goods';

    protected $fillable = [
        'code',
        'name',
        'stock',
        'item_category_id',
        
    ];
}
