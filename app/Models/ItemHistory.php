<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemHistory extends Model
{
    use HasFactory;
    protected $table= 'item_history';

    protected $fillable = [
        'date_time',
        'item_category_id',
        'goods_id',
        'type',
        'remaks',
        'before_amount',
        'out',
        'in',
        'adjus',
        'after_amount',
        'create_by',
    ];
}
